CC=gcc
MPICC=mpicc
SRC=src/
BIN=bin/
SER=gol_serial
PAR=gol_parallel
LDFLAGS= -lrt

all: $(SER) $(PAR)

$(SER): $(SRC)$(SER).c
	$(CC) -o $(BIN)$@ $< $(LDFLAGS)

$(PAR): $(SRC)$(PAR).c
	$(MPICC) -o $(BIN)$@ $< $(LDFLAGS)

.PHONY: clean
 
clean:
	rm -f $(BIN)$(SER) $(BIN)$(PAR) *.o