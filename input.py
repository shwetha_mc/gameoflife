#!/usr/bin/python

import sys, random

if (len(sys.argv)!=5):
	print "Usage: ", sys.argv[0], " m n <no_of_gens> output_file";
	exit();

m=sys.argv[1];
n=sys.argv[2];
g=sys.argv[3];

#open the input file.
f=open(sys.argv[4], 'w');
f.write(m + " " + n + "\n" + g + "\n");

m=int(m);
n=int(n);
g=int(g);

#now generate instance and write to file.
for i in range(m):
	for j in range(n):
		r = random.randint(0,1);
		f.write(str(r)+" ");
	f.write("\n");

f.close();
