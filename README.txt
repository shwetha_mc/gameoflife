Build directions:

First clean the binaries from base dir-
make clean 

Build from base dir -
make

Generate a random input from base dir-
./input.py <rows> <cols> <generations> bin/<inputfilename>

Run from bin dir-
Serial version:
./gol_serial <inputfilename> <outputfilename>

Parallel version:
See pbs script.