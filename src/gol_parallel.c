#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#define DIM 2
#define DATA_TAG 67
#define SIZE_TAG 64
#define TOP 10
#define BOTTOM 11
#define LEFT 12
#define RIGHT 13
#define NW 14
#define SW 15
#define NE 16
#define SE 17


void print_usage()
{
	printf("Usage\n");
	printf("./<executable> <input_file> <output_file> \n");
	exit(0);
}


void print_arr(int m, int n, int* arr)
{
	int i,j;
	for(i=0;i<m;++i) {
		for(j=0;j<n;++j)
			printf("%d ",*(arr+(i*n)+j));
		printf("\n");
	}
}

int get_neigh(int *arr, int i, int j, int m, int n)
{
	if(i==0){
		if(j==0)
			return (*(arr+(i*n)+(j+1))+(*(arr+((i+1)*n)+j))+(*(arr+((i+1)*n)+j+1)));
		else if(j==(n-1))
			return (*(arr+(i*n)+j-1)+(*(arr+((i+1)*n)+j))+(*(arr+((i+1)*n)+j-1)));
		else
			return (*(arr+(i*n)+j+1)+(*(arr+(i*n)+j-1))+(*(arr+((i+1)*n)+j-1))+(*(arr+((i+1)*n)+j))+(*(arr+((i+1)*n)+j+1)));
	}
	else if(i==(m-1))
	{
		if(j==0)
			return (*(arr+(i*n)+j+1)+(*(arr+((i-1)*n)+j))+(*(arr+((i-1)*n)+j+1)));
		else if(j==(n-1))
			return (*(arr+(i*n)+j-1)+(*(arr+((i-1)*n)+j-1))+(*(arr+((i-1)*n)+j)));
		else
			return (*(arr+(i*n)+j-1)+(*(arr+(i*n)+j+1))+(*(arr+((i-1)*n)+j-1))+(*(arr+((i-1)*n)+j))+(*(arr+((i-1)*n)+j+1)));

	}
	else
	{
		if(j==0)
			return (*(arr+(i*n)+j+1)+(*(arr+((i-1)*n)+j))+(*(arr+((i-1)*n)+j+1))+(*(arr+((i+1)*n)+j))+(*(arr+((i+1)*n)+j+1)));
		else if(j==(n-1))
			return (*(arr+(i*n)+j-1)+(*(arr+((i-1)*n)+j))+(*(arr+((i-1)*n)+j-1))+(*(arr+((i+1)*n)+j))+(*(arr+((i+1)*n)+j-1)));
		else
			return (*(arr+(i*n)+j-1)+(*(arr+(i*n)+j+1))+(*(arr+((i-1)*n)+j-1))+(*(arr+((i-1)*n)+j))+(*(arr+((i-1)*n)+j+1))+(*(arr+((i+1)*n)+j-1))+(*(arr+((i+1)*n)+j))+(*(arr+((i+1)*n)+j+1)));
	}
}


void get_count_inner(int *arr, int *count_arr,int m, int n)
{
	int i,j;
	for(i=0;i<m;++i){
		for(j=0;j<n;++j){
			*(count_arr+(i*n)+j)=get_neigh(arr,i,j,m,n);
		}
	}
}

void get_count_borders(int *arr, int *count_arr,int m, int n, int border, int *buf)
{
	int i,j;
	switch(border) {
		case TOP:		{
							/*int new_arr[m+1][n];
							for(i=0;i<n;++i)	
								new_arr[0][i]=*(buf+i);
							for(i=1;i<=m;i++)
								for(j=0;j<n;j++)
									new_arr[i][j]=*(arr+((i-1)*n)+j);*/
							for(i=0;i<(n);++i)
							{
								if(*(buf+i)==1){
								*(count_arr+i)+=1;
								if((i+1)<=(n-1))
								*(count_arr+i+1)+=1;
								if((i-1)>=0)
									*(count_arr+i-1)+=1;
								//get_neigh(&new_arr[0][0],1,i,m+1,n);
								}					
							}
						break;
						}
		case BOTTOM:	{
							/*int new_arr[m+1][n];
							for(i=0;i<m;++i)
								for(j=0;j<n;++j)
					 				new_arr[i][j]=*(arr+(i*n)+j);
					 		for(i=0;i<n;++i)
					 			new_arr[m][i]=*(buf+i);*/
						 	for(i=0;i<n;++i)
						 	{
						 		if(*(buf+i)==1){
						 		*(count_arr+((m-1)*n)+i)+=1;
						 		if((i+1)<=(n-1))
								*(count_arr+((m-1)*n)+i+1)+=1;
								if((i-1)>=0)
									*(count_arr+((m-1)*n)+i-1)+=1;
							}
								
						 	//get_neigh(&new_arr[0][0],m-1,i,m+1,n);

						 	}
						 	break;
						}
		case LEFT:		{

							/*int new_arr[m][n+1];
							for(i=0;i<m;++i)
								new_arr[i][0]=*(buf+i);
							for(i=0;i<m;++i)
								for(j=1;j<=n;++j)
									new_arr[i][j]=*(arr+(i*n)+j-1);*/
							for(i=0;i<m;++i)
							{
								if(*(buf+i)==1){
						 			*(count_arr+(i*n))+=1;
						 		if((i+1)<=(m-1))
						 			*(count_arr+((i+1)*n))+=1;
						 		if((i-1)>=0)
						 			*(count_arr+((i-1)*n))+=1;
						 		}	
								//get_neigh(&new_arr[0][0],i,1,m,n+1);
							}
							break;
						}
		case RIGHT:		{
							/*int new_arr[m][n+1];
							for(i=0;i<m;++i)
								for(j=0;j<n;++j)
									new_arr[i][j]=*(arr+(i*n)+j);
							for(i=0;i<m;++i)
								new_arr[i][n]=*(buf+i);*/
							for(i=0;i<m;++i)
							{
								if(*(buf+i)==1){
								*(count_arr+(i*n)+n-1)+=1;
								if((i+1)<=(m-1))
						 		*(count_arr+((i+1)*n)+n-1)+=1;
						 		if((i-1)>=0)
						 			*(count_arr+((i-1)*n)+n-1)+=1;
							}
						}

							//get_neigh(&new_arr[0][0],i,n-1,m,n+1);
							break;
						}

	}
	
}

void get_count_corners(int *arr, int *count_arr,int m, int n, int corner, int value)
{
	int i,j;
	switch(corner){
		case NE:	if(value==1) {*(count_arr+n-1)+=1;} break;
		case SE:	if(value==1) {*(count_arr+((m-1)*n)+n-1)+=1;} break;
		case NW:	if(value==1) {*(count_arr)+=1;} break;
		case SW: 	if(value==1) {*(count_arr+((m-1)*n))+=1;} break;

	}
}

void get_count(int *arr, int *count_arr,int m, int n)
{
	int i,j;
	for(i=0;i<m;++i){
		for(j=0;j<n;++j){
			*(count_arr+(i*n)+j)=get_neigh(arr,i,j,m,n);
		}
	}
}

void arr_cpy(int *src, int *dest, int rows, int cols)
{
	int i,j;
	//printf("%d\n",__LINE__ );
	for(i=0;i<rows;++i){
		for(j=0;j<cols;++j)
			*(dest+(i*cols)+j)=(*(src+(i*cols)+j));
	}
}
int main(int argc, char *argv[])
{
	if(argc!=3)
		print_usage();
	else{
		struct timeval start, end;
		
		//Initialize MPI
		MPI_Init(&argc,&argv);
		int rank,size;
		MPI_Comm_size(MPI_COMM_WORLD,&size);
		MPI_Comm_rank(MPI_COMM_WORLD,&rank);
		int dims[2];
		int period[2];
		period[0]=1;
		period[1]=1;
		//MPI_Dims_create(size,2,dims);
		dims[0]=0;
		dims[1]=0;
		MPI_Dims_create(size,2,dims);
		MPI_Comm cart_comm;
		MPI_Cart_create(MPI_COMM_WORLD,2,dims,period,1,&cart_comm);
		//printf("dimensions %d %d \n",dims[0],dims[1] );
		int new_m, new_n,dest;
		int rowsall[dims[0]];
		int colsall[dims[1]];
		int rowsall_ofst[dims[0]];
		int colsall_ofst[dims[1]];
		
		int k,g;
		int i,j;
		MPI_Status stat;
		if(rank==0) {
			FILE *inp;
			
			int c; 
			inp=fopen(argv[1],"r");
			int m,n;
				
			fscanf(inp,"%d %d\n",&m,&n);
			fscanf(inp,"%d\n",&g);
			int arr[m][n];
			
			for(i=0;i<m;++i)
			{
				for(j=0;j<n;++j) {
					fscanf(inp,"%d ",&c);
					arr[i][j]=c;
				}
				fscanf(inp,"\n");
			}
		
			fclose(inp);

			/*Distribute data to procs*/
			gettimeofday(&start,NULL);
			int p,p_row, p_col, p_row_rem, p_col_rem,numrows,numcols,r,s;
			
			p_row=m/dims[0];
			p_row_rem=m%dims[0];
			p_col=n/dims[1];
			p_col_rem=n%dims[1];
			//int new_arr[new_m][new_n];
			numrows=p_row;
			numcols=p_col;
			//printf("Dimensions %d %d p_row %d p_col %d p_col_rem %d p_row_rem %d\n",dims[0],dims[1],p_row,p_col,p_col_rem,p_row_rem );
			int row_start=0, col_start=0,w;

			for (i = 0; i < dims[0]; ++i)
			{
				if(i<p_row_rem)
					numrows=p_row+1;
				
				for(j=0;j<dims[1];++j)
				{
					int cart_coord[2]={i,j};
					MPI_Cart_rank(cart_comm,cart_coord,&dest);
					//printf("%d %d\n",__LINE__, dest);
					if(j<p_col_rem)
						numcols=p_col+1;
					colsall[j]=numcols;
					rowsall[i]=numrows;
					MPI_Send(&numrows,1,MPI_INT,dest,SIZE_TAG,cart_comm);

					MPI_Send(&numcols,1,MPI_INT,dest,SIZE_TAG,cart_comm);
					MPI_Send(&g,1,MPI_INT,dest,SIZE_TAG,cart_comm);
					for(p=0;p<numrows;++p)
					{
										
						MPI_Send(&arr[row_start+p][col_start],numcols,MPI_INT,dest,DATA_TAG,cart_comm);

						/*printf("col_start %d\n",col_start );
						for(w=0;w<numcols;++w)
							printf("%d ", arr[row_start+p][col_start+w]);
						printf("\n");
*/
					}
					col_start+=numcols;
				}
				row_start+=numrows;
				col_start=0;


			}
		
		/*	MPI_Recv(&new_m,1,MPI_INT,0,SIZE_TAG,cart_comm,&stat);
			printf("%d %d\n",__LINE__, rank);
			MPI_Recv(&new_n,1,MPI_INT,0,SIZE_TAG,cart_comm, &stat);
			printf("%d %d\n",__LINE__, rank);
			//int new_arr[new_m][new_n];
		*/	
			
		/*	for(r=0;r<new_m;++r)
				MPI_Recv(new_arr[r],new_n,MPI_INT,0,DATA_TAG,cart_comm,&stat);
		*/	
			/*End of data distribution*/		
			}
		/*else {
			MPI_Status stat;
			int i;*/
			//int new_m,new_n,i;

			MPI_Recv(&new_m,1,MPI_INT,0,SIZE_TAG,cart_comm,&stat);
			MPI_Recv(&new_n,1,MPI_INT,0,SIZE_TAG,cart_comm, &stat);
			MPI_Recv(&g,1,MPI_INT,0,SIZE_TAG,cart_comm, &stat);
			//printf("%d %d\n",__LINE__, rank);			
			int new_arr[new_m][new_n],temp[new_m][new_n];
			for(i=0;i<new_m;++i)
				MPI_Recv(new_arr[i],new_n,MPI_INT,0,DATA_TAG,cart_comm,&stat);
		//	printf("%d %d\n", __LINE__,rank);
			MPI_Barrier(cart_comm);	
		//	printf("%d %d\n",__LINE__, rank);
		//	print_arr(new_m,new_n,&new_arr[0][0]);
			int count_arr[new_m][new_n];

			for(i=0;i<new_m;++i)
				for(j=0;j<new_n;++j)
					count_arr[i][j]=0;

		//}
			//printf("%d\n",rank );
			//print_arr(new_m,new_n,&new_arr[0][0]);
			//now solve game of life
			//Persistent Sends and Receives
			int send_buf_top[new_n], recv_buf_top[new_n],  send_ne, recv_ne, send_nw, recv_nw, send_sw, recv_sw, send_se, recv_se, send_buf_bottom[new_n],  recv_buf_bottom[new_n], recv_buf_left[new_m], recv_buf_right[new_m];
			MPI_Request req_send_top, req_send_bottom, req_send_nw, req_recv_nw, req_recv_top, req_recv_bottom, req_send_left, req_recv_left, req_send_right, req_recv_right, req_send_ne, req_recv_ne, req_send_sw, req_recv_sw, req_send_se, req_recv_se;
			MPI_Datatype send_buf_right, send_buf_left;
			int ne, nw, se, sw, top, bottom, left, right;
			int cart_coords[2];
			//int dest_cart[2];
		//	printf("%d %d\n",__LINE__, rank);
			MPI_Cart_coords(cart_comm,rank,2,cart_coords);
		//	printf("Cart coords %d %d\n",cart_coords[0],cart_coords[1] );
			if(cart_coords[1]!=(dims[1]-1))
			{
				MPI_Type_vector(new_m, 1, new_n, MPI_INT, &send_buf_right);
				MPI_Type_commit(&send_buf_right);
				int dest_cart[2]={cart_coords[0], cart_coords[1]+1};
				MPI_Cart_rank(cart_comm, dest_cart, &dest);
				MPI_Send_init(&new_arr[0][new_n-1],1,send_buf_right,dest,DATA_TAG, cart_comm, &req_send_right);	
				MPI_Recv_init(&recv_buf_right, new_m, MPI_INT, dest, DATA_TAG, cart_comm, &req_recv_right);
				if(cart_coords[0]!=(dims[0]-1) )
				{
					int dest_cart[2]={cart_coords[0]+1,cart_coords[1]};
					MPI_Cart_rank(cart_comm, dest_cart, &dest);
					MPI_Send_init(&new_arr[new_m-1][0],new_n, MPI_INT, dest, DATA_TAG,cart_comm, &req_send_bottom);
					MPI_Recv_init(&recv_buf_bottom,new_n, MPI_INT, dest,DATA_TAG, cart_comm, &req_recv_bottom);
					int dest_cart1[2]={cart_coords[0]+1,cart_coords[1]+1};
					MPI_Cart_rank(cart_comm, dest_cart1, &dest);
					MPI_Send_init(&new_arr[new_m-1][new_n-1],1, MPI_INT, dest, DATA_TAG,cart_comm, &req_send_se);
					MPI_Recv_init(&recv_se,1, MPI_INT, dest, DATA_TAG, cart_comm, &req_recv_se);
				}
				if(cart_coords[0]!=0)
				{
					int dest_cart[2]={cart_coords[0]-1,cart_coords[1]};
					MPI_Cart_rank(cart_comm, dest_cart, &dest);
					MPI_Send_init(&new_arr[0][0],new_n, MPI_INT, dest, DATA_TAG,cart_comm, &req_send_top);
					MPI_Recv_init(&recv_buf_top,new_n, MPI_INT, dest,DATA_TAG, cart_comm, &req_recv_top);
					int dest_cart1[2]={cart_coords[0]-1,cart_coords[1]+1};
					MPI_Cart_rank(cart_comm, dest_cart1, &dest);
					MPI_Send_init(&new_arr[0][new_n-1],1, MPI_INT, dest,DATA_TAG, cart_comm, &req_send_ne);
					MPI_Recv_init(&recv_ne,1, MPI_INT, dest,  DATA_TAG,cart_comm,&req_recv_ne);	
				}
			}
			if(cart_coords[1]!=0)
			{
				MPI_Type_vector(new_m, 1, new_n, MPI_INT, &send_buf_left);
				MPI_Type_commit(&send_buf_left);
				int dest_cart[2]={cart_coords[0], cart_coords[1]-1};
				MPI_Cart_rank(cart_comm, dest_cart, &dest);
				MPI_Send_init(&new_arr[0][0],1,send_buf_left,dest,DATA_TAG, cart_comm, &req_send_left);	
				MPI_Recv_init(&recv_buf_left, new_m, MPI_INT, dest, DATA_TAG, cart_comm, &req_recv_left);
				if(cart_coords[0]!=(dims[0]-1) )
				{
					int dest_cart[2]={cart_coords[0]+1,cart_coords[1]};
					MPI_Cart_rank(cart_comm, dest_cart, &dest);
					MPI_Send_init(&new_arr[new_m-1][0],new_n, MPI_INT, dest,DATA_TAG, cart_comm, &req_send_bottom);
					MPI_Recv_init(&recv_buf_bottom,new_n, MPI_INT, dest, DATA_TAG,cart_comm, &req_recv_bottom);
					int dest_cart1[2]={cart_coords[0]+1,cart_coords[1]-1};
					MPI_Cart_rank(cart_comm, dest_cart1, &dest);
					MPI_Send_init(&new_arr[new_m-1][0],1, MPI_INT, dest, DATA_TAG,cart_comm, &req_send_sw);
					MPI_Recv_init(&recv_sw,1, MPI_INT, dest, DATA_TAG,cart_comm, &req_recv_sw);
				}
				if(cart_coords[0]!=0)
				{
					int dest_cart[2]={cart_coords[0]-1,cart_coords[1]};
					MPI_Cart_rank(cart_comm, dest_cart, &dest);
					MPI_Send_init(&new_arr[0][0],new_n, MPI_INT, dest,DATA_TAG, cart_comm, &req_send_top);
					MPI_Recv_init(&recv_buf_top,new_n, MPI_INT, dest, DATA_TAG,cart_comm, &req_recv_top);
					int dest_cart1[2]={cart_coords[0]-1,cart_coords[1]-1};
					MPI_Cart_rank(cart_comm, dest_cart1, &dest);
					MPI_Send_init(&new_arr[0][0],1, MPI_INT, dest,DATA_TAG, cart_comm, &req_send_nw);
					MPI_Recv_init(&recv_nw,1, MPI_INT, dest, DATA_TAG,cart_comm, &req_recv_nw);	
				}	
				
			}
			//printf("%d %d\n",__LINE__, rank);

			for(k=0;k<g;++k)
			{
				//printf("%d %d\n",__LINE__, rank);
				//populate matrix of counts of neighbors
				/*Startup persistent communication*/
				if(cart_coords[1]!=(dims[1]-1))
				{
					MPI_Start(&req_send_right);
					MPI_Start(&req_recv_right);
					if(cart_coords[0]!=(dims[0]-1) )
					{
						MPI_Start(&req_send_bottom);
						MPI_Start( &req_recv_bottom);
						MPI_Start(&req_send_se);
						MPI_Start( &req_recv_se);
					}
					if(cart_coords[0]!=0)
					{
						MPI_Start(&req_send_top);
						MPI_Start(&req_recv_top);
						MPI_Start(&req_send_ne);
						MPI_Start( &req_recv_ne);
					}
				}
				//printf("%d %d\n",__LINE__, rank);
				if(cart_coords[1]!=0)
				{
					MPI_Start(&req_send_left);
					MPI_Start(&req_recv_left);
					if(cart_coords[0]!=(dims[0]-1) )
					{
						MPI_Start(&req_send_bottom);
						MPI_Start(&req_recv_bottom);
						MPI_Start(&req_send_sw);
						MPI_Start(&req_recv_sw);
					}
					if(cart_coords[0]!=0)
					{
						MPI_Start(&req_send_top);
						MPI_Start(&req_recv_top);
						MPI_Start(&req_send_nw);
						MPI_Start(&req_recv_nw);
					}
				}
				/*End communication*/
				/*Compute innards if there are more than 2 rows or columns*/
				//if(new_m>2 && new_n>2)
					get_count_inner(&new_arr[0][0],&count_arr[0][0],new_m,new_n);
				
				/*Otherwise wait for receive before computing border*/
				//else
				//{
					//printf("%d %d\n",__LINE__, rank);
					//printf("coords %d %d dims %d %d\n",cart_coords[0],cart_coords[1],dims[0],dims[1]);
					if(cart_coords[1]!=(dims[1]-1))
					{
					//	MPI_Wait(&req_send_right,MPI_STATUS_IGNORE);
						MPI_Wait(&req_recv_right,MPI_STATUS_IGNORE);
						get_count_borders(&new_arr[0][0],&count_arr[0][0],new_m,new_n,RIGHT,&recv_buf_right[0]);	
					//	printf("%d %d\n",__LINE__, rank);
					//	printf("Right\n");
					//	print_arr(new_m,1,&recv_buf_right[0]);
					//	print_arr(new_m,new_n,&count_arr[0][0]);
						if(cart_coords[0]!=(dims[0]-1))
						{
					//		MPI_Wait(&req_send_bottom,MPI_STATUS_IGNORE);
							MPI_Wait(&req_recv_bottom,MPI_STATUS_IGNORE);
							get_count_borders(&new_arr[0][0],&count_arr[0][0],new_m,new_n,BOTTOM,&recv_buf_bottom[0]);	
					//		printf("Bottom\n");
					//		print_arr(1,new_n,&recv_buf_bottom[0]);
					//		print_arr(new_m,new_n,&count_arr[0][0]);
					//		MPI_Wait(&req_send_se,MPI_STATUS_IGNORE);
							MPI_Wait(&req_recv_se,MPI_STATUS_IGNORE);
							get_count_corners(&new_arr[0][0],&count_arr[0][0],new_m,new_n,SE,recv_se);
					//		printf("SE %d\n",recv_se);
					//		print_arr(new_m,new_n,&count_arr[0][0]);
						}
						if(cart_coords[0]!=0)
						{
					//		MPI_Wait(&req_send_top,MPI_STATUS_IGNORE);
							MPI_Wait(&req_recv_top,MPI_STATUS_IGNORE);
							get_count_borders(&new_arr[0][0],&count_arr[0][0],new_m,new_n,TOP,&recv_buf_top[0]);	
					//		printf("Top\n");
					//		print_arr(1,new_n,&recv_buf_top[0]);
					//		print_arr(new_m,new_n,&count_arr[0][0]);
					//		MPI_Wait(&req_send_ne,MPI_STATUS_IGNORE);
							MPI_Wait(&req_recv_ne,MPI_STATUS_IGNORE);
							get_count_corners(&new_arr[0][0],&count_arr[0][0],new_m,new_n,NE,recv_ne);
					//		printf("NE %d\n",recv_ne);
					//		print_arr(new_m,new_n,&count_arr[0][0]);
						}
					}
					if(cart_coords[1]!=0)
					{
					//	printf("%d %d\n",__LINE__, rank);
						//MPI_Wait(&req_send_left,MPI_STATUS_IGNORE);
						MPI_Wait(&req_recv_left,MPI_STATUS_IGNORE);
						get_count_borders(&new_arr[0][0],&count_arr[0][0],new_m,new_n,LEFT,&recv_buf_left[0]);	
					//	printf("%d %d\n",__LINE__, rank);
					//	printf("Left\n");
					//	print_arr(new_m,1,&recv_buf_left[0]);
					//	print_arr(new_m,new_n,&count_arr[0][0]);
						if(cart_coords[0]!=(dims[0]-1) )
						{
							//MPI_Wait(&req_send_bottom,MPI_STATUS_IGNORE);
							MPI_Wait(&req_recv_bottom,MPI_STATUS_IGNORE);
							if(cart_coords[1]==(dims[1]-1))
							get_count_borders(&new_arr[0][0],&count_arr[0][0],new_m,new_n,BOTTOM,&recv_buf_bottom[0]);
					//		printf("Bottom %d\n",__LINE__);	
					//		print_arr(1,new_n,&recv_buf_bottom[0]);
					//		print_arr(new_m,new_n,&count_arr[0][0]);
							//MPI_Wait(&req_send_sw,MPI_STATUS_IGNORE);
							MPI_Wait(&req_recv_sw,MPI_STATUS_IGNORE);
							get_count_corners(&new_arr[0][0],&count_arr[0][0],new_m,new_n,SW,recv_sw);
					//		printf("SW %d\n",recv_sw);
					//		print_arr(new_m,new_n,&count_arr[0][0]);
						}
						if(cart_coords[0]!=0)
						{
						//	MPI_Wait(&req_send_top,MPI_STATUS_IGNORE);
							MPI_Wait(&req_recv_top,MPI_STATUS_IGNORE);
							if(cart_coords[1]==(dims[1]-1))
							get_count_borders(&new_arr[0][0],&count_arr[0][0],new_m,new_n,TOP,&recv_buf_top[0]);	
					//		printf("Top %d\n",__LINE__);
					//		print_arr(1,new_n,&recv_buf_top[0]);
					//		print_arr(new_m,new_n,&count_arr[0][0]);
							//MPI_Wait(&req_send_nw,MPI_STATUS_IGNORE);
							MPI_Wait(&req_recv_nw,MPI_STATUS_IGNORE);
							get_count_corners(&new_arr[0][0],&count_arr[0][0],new_m,new_n,NW,recv_nw);
					//		printf("NW %d\n", recv_nw);
					//		print_arr(new_m,new_n,&count_arr[0][0]);
						}
					}

					
				//}
				
				//print_arr(m,n,&count_arr[0][0]);
				//printf("%d %d\n",new_m,new_n );
				for(i=0;i<new_m;++i)
				{
					for(j=0;j<new_n;++j)
					{
				//		printf("%d %d\n",rank,__LINE__ );
						if(new_arr[i][j]==1) {
						 	if((count_arr[i][j]<2)||(count_arr[i][j]>3))
								temp[i][j]=0;
							else
								temp[i][j]=new_arr[i][j];
						}
						else {
							if(count_arr[i][j]==3)
								temp[i][j]=1;
							else
								temp[i][j]=0;
						}
					}
				}
				arr_cpy(&temp[0][0],&new_arr[0][0],new_m,new_n);
		//	print_arr(m,n,&temp[0][0]);
			}
		/*	for(i=0;i<m;++i) {
				for(j=0;j<n;++j)
					fprintf(out,"%d ",arr[i][j]);
				fprintf(out,"\n");
			}

		fclose(out);*/
			//printf("%d %d\n",__LINE__, rank);
		//print_arr(new_m,new_n,&new_arr[0][0]);
		if(rank==0) {
			rowsall_ofst[0]=0;
			colsall_ofst[0]=0;
		for(i=0;i<dims[0];++i) {
				rowsall_ofst[i+1]=rowsall_ofst[i]+rowsall[i];
		//		printf("%d \t",rowsall_ofst[i] );
		//	printf("\n");
		}


		
			for(j=0;j<dims[1];++j){
				colsall_ofst[j+1]=colsall_ofst[j]+colsall[j];
		//		printf("%d \t",colsall_ofst[j] );
		//	printf("\n");
		}


	}

		for(i=0;i<new_m;++i)
			MPI_Send(new_arr[i],new_n,MPI_INT,0,DATA_TAG,cart_comm);
			
		
		if(rank==0)
		{
			int m,n;
			n=colsall_ofst[dims[1]-1]+colsall[dims[1]-1];
			m=rowsall_ofst[dims[0]-1]+rowsall[dims[0]-1];
			int op_arr[m][n];
			int destrecv;
			FILE *out;
			out=fopen(argv[2],"w");
			int w;
			for(i=0;i<dims[0];++i){
				for(j=0;j<dims[1];++j) {
					int cartco[2]={i,j};
					MPI_Cart_rank(cart_comm, cartco,&destrecv);
		//			printf("%d %d %d\n",rank,destrecv,rowsall[i] );
					for(w=0;w<rowsall[i];++w){
		//				printf("%d %d \n",rank,destrecv );
						MPI_Recv(&op_arr[rowsall_ofst[i]+w][colsall_ofst[j]],colsall[j],MPI_INT,destrecv,DATA_TAG,cart_comm,&stat);
					}
				}
			}
			for(i=0;i<m;++i){
				for(j=0;j<n;++j)
					fprintf(out, "%d ",op_arr[i][j] );
				fprintf(out, "\n" );
			}
		}

		


		MPI_Finalize();
		gettimeofday(&end,NULL);
		if(rank==0)
		printf("Elapsed time %ld\n",(((end.tv_sec*1000000)+end.tv_usec)-((start.tv_sec*1000000)+start.tv_usec) ));
	}

	return 0;
}