#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

void print_usage()
{
	printf("Usage\n");
	printf("./<executable> <input_file> <output_file> \n");
	exit(0);
}


void print_arr(int m, int n, int* arr)
{
	int i,j;
	for(i=0;i<m;++i) {
		for(j=0;j<n;++j)
			printf("%d ",*(arr+(i*n)+j));
		printf("\n");
	}
}

int get_neigh(int *arr, int i, int j, int m, int n)
{
	if(i==0){
		if(j==0)
			return (*(arr+(i*n)+(j+1))+(*(arr+((i+1)*n)+j))+(*(arr+((i+1)*n)+j+1)));
		else if(j==(n-1))
			return (*(arr+(i*n)+j-1)+(*(arr+((i+1)*n)+j))+(*(arr+((i+1)*n)+j-1)));
		else
			return (*(arr+(i*n)+j+1)+(*(arr+(i*n)+j-1))+(*(arr+((i+1)*n)+j-1))+(*(arr+((i+1)*n)+j))+(*(arr+((i+1)*n)+j+1)));
	}
	else if(i==(m-1))
	{
		if(j==0)
			return (*(arr+(i*n)+j+1)+(*(arr+((i-1)*n)+j))+(*(arr+((i-1)*n)+j+1)));
		else if(j==(n-1))
			return (*(arr+(i*n)+j-1)+(*(arr+((i-1)*n)+j-1))+(*(arr+((i-1)*n)+j)));
		else
			return (*(arr+(i*n)+j-1)+(*(arr+(i*n)+j+1))+(*(arr+((i-1)*n)+j-1))+(*(arr+((i-1)*n)+j))+(*(arr+((i-1)*n)+j+1)));

	}
	else
	{
		if(j==0)
			return (*(arr+(i*n)+j+1)+(*(arr+((i-1)*n)+j))+(*(arr+((i-1)*n)+j+1))+(*(arr+((i+1)*n)+j))+(*(arr+((i+1)*n)+j+1)));
		else if(j==(n-1))
			return (*(arr+(i*n)+j-1)+(*(arr+((i-1)*n)+j))+(*(arr+((i-1)*n)+j-1))+(*(arr+((i+1)*n)+j))+(*(arr+((i+1)*n)+j-1)));
		else
			return (*(arr+(i*n)+j-1)+(*(arr+(i*n)+j+1))+(*(arr+((i-1)*n)+j-1))+(*(arr+((i-1)*n)+j))+(*(arr+((i-1)*n)+j+1))+(*(arr+((i+1)*n)+j-1))+(*(arr+((i+1)*n)+j))+(*(arr+((i+1)*n)+j+1)));
	}
}


void get_count(int *arr, int *count_arr,int m, int n)
{
	int i,j;
	for(i=0;i<m;++i){
		for(j=0;j<n;++j){
			*(count_arr+(i*n)+j)=get_neigh(arr,i,j,m,n);
		}
	}
}

void arr_cpy(int *src, int *dest, int rows, int cols)
{
	int i,j;
	for(i=0;i<rows;++i){
		for(j=0;j<cols;++j)
			*(dest+(i*cols)+j)=(*(src+(i*cols)+j));
	}
}
int main(int argc, char *argv[])
{
	if(argc!=3)
		print_usage();
	else{
		struct timeval start, end;
		FILE *inp, *out;
		int m,n;
		int g,i,j,k;
		int c; 
		inp=fopen(argv[1],"r");
		out=fopen(argv[2],"w");
		fscanf(inp,"%d %d\n",&m,&n);
		fscanf(inp,"%d\n",&g);
		int arr[m][n], temp[m][n];
		int count_arr[m][n];
		for(i=0;i<m;++i)
		{
			for(j=0;j<n;++j) {
				fscanf(inp,"%d ",&c);
				arr[i][j]=c;
			}
			fscanf(inp,"\n");
		}
		
		fclose(inp);
		gettimeofday(&start,NULL);
		//print_arr(m,n,&arr[0][0]);
		//now solve game of life
		for(k=0;k<g;++k)
		{
			//populate matrix of counts of neighbors
			get_count(&arr[0][0],&count_arr[0][0],m,n);
		//	print_arr(m,n,&count_arr[0][0]);
			for(i=0;i<m;++i)
			{
				for(j=0;j<n;++j)
				{
					if(arr[i][j]==1) {
					 	if((count_arr[i][j]<2)||(count_arr[i][j]>3))
							temp[i][j]=0;
						else
							temp[i][j]=arr[i][j];
					}
					else {
						if(count_arr[i][j]==3)
							temp[i][j]=1;
						else
							temp[i][j]=0;
					}
				}
			}
			arr_cpy(&temp[0][0],&arr[0][0],m,n);
		//	print_arr(m,n,&temp[0][0]);
		}
		for(i=0;i<m;++i) {
			for(j=0;j<n;++j)
				fprintf(out,"%d ",arr[i][j]);
			fprintf(out,"\n");
		}

		fclose(out);
		gettimeofday(&end,NULL);
		//printf("%ld %ld\n",end.tv_usec,start.tv_usec );
		printf("%ld\n",(((end.tv_sec*1000000)+end.tv_usec)-((start.tv_sec*1000000)+start.tv_usec) ));
		//print_arr(m,n,&arr[0][0]);
	}
	return 0;
}